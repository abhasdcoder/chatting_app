-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: msgapp
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chatgroup`
--

DROP TABLE IF EXISTS `chatgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chatgroup` (
  `id` int NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chatgroup`
--

LOCK TABLES `chatgroup` WRITE;
/*!40000 ALTER TABLE `chatgroup` DISABLE KEYS */;
INSERT INTO `chatgroup` VALUES (1,'Society Maintenance'),(2,'Culural Events'),(3,'Jethalal Problems'),(4,'Abdul ka Soda'),(5,'Popatlal ki Shaadi'),(6,'Bhide ki Paathshala');
/*!40000 ALTER TABLE `chatgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msglog`
--

DROP TABLE IF EXISTS `msglog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `msglog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `message` varchar(500) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `userid` int NOT NULL DEFAULT '1',
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `msglog_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`),
  CONSTRAINT `msglog_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `chatgroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=480 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msglog`
--

LOCK TABLES `msglog` WRITE;
/*!40000 ALTER TABLE `msglog` DISABLE KEYS */;
INSERT INTO `msglog` VALUES (453,'Hello Mehtaasaab!','2021-04-04 16:55:08',1,1),(454,'Hey Bheede! What is up?','2021-04-04 16:55:22',2,1),(455,'Its Aatmaram Tukaram Bheede please','2021-04-04 16:55:36',1,1),(456,'Hey','2021-04-04 16:58:15',2,1),(457,'No issues','2021-04-04 16:58:40',2,1),(458,'I will call you Master Bhide now on','2021-04-04 16:58:53',2,1),(459,'Mehtasaab ... you are so Funny','2021-04-04 16:59:14',1,1),(460,'btw Bhide','2021-04-04 16:59:30',2,1),(461,'Did Jethala pay the monthly mintenance??','2021-04-04 16:59:42',2,1),(462,'No..As usual','2021-04-04 17:00:02',1,1),(463,'Oh','2021-04-04 17:00:35',2,1),(464,'Who else is in the list','2021-04-04 17:00:43',2,1),(465,'Not many','2021-04-04 17:00:47',1,1),(466,'Mr Gada is usually a consistent defaulter','2021-04-04 17:01:00',1,1),(467,'apart from that none','2021-04-04 17:01:07',1,1),(468,'like once or twice maybe Popatlal','2021-04-04 17:01:14',1,1),(469,'ohh','2021-04-04 17:01:18',2,1),(470,'we need to teach jethala some lesson','2021-04-04 17:01:28',1,1),(471,'Oh Bhide, just chill','2021-04-04 17:01:35',2,1),(472,'Well discuss this in provate','2021-04-04 17:01:50',2,1),(473,'remember, here everyone is reading us','2021-04-04 17:02:01',2,1),(474,'Oh ...hahaha yes.. got it','2021-04-04 17:02:10',1,1),(475,'alright bhide','2021-04-04 17:02:16',2,1),(476,'hope you see a good growth this season','2021-04-04 17:02:33',2,1),(477,'??','2021-04-04 17:02:37',1,1),(478,'oops... nothin ','2021-04-04 17:02:51',2,1),(479,'bubbyye','2021-04-04 17:02:55',2,1);
/*!40000 ALTER TABLE `msglog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `age` int NOT NULL,
  `city` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin',30,'Chopda','M','admin','abcd1234'),(2,'Mehtasaab',40,'Lucknow','M','Mehtasaab','abcd1234'),(3,'Anjali Mehta',38,'Mumbai','F','Anjali','abcd1234'),(4,'Komal Bhabi',42,'Pune','F','Komal','abcd1234');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-04 22:55:50
