var mysql = require('mysql')
const express = require('express')
const app = express()
const port = 3000
const path= require("path");
var cors = require('cors')

app.use(cors())

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

var dbCreds = {
  host: 'localhost',
  user: 'vishwas',
  password: 'Vishwas@1234',
  database: 'msgapp'
};

function getConn(){
  var connection = mysql.createConnection(dbCreds);
  return connection;
}

//Route to make the app and request run on the same server localhost and run with a userid
app.get ('/chatsuc',(req,res)=> {
  var connection = getConn();

  var x=req.query.userid;
  connection.connect()

  var checkuser =("select id from users where id="+x);

  connection.query( checkuser, function (err, rows, fields) {
    //if (err) throw err; 
    console.log(rows);

    if (rows.length==0 || err) {
      // rows={id:"ERROR"};
      // rows=JSON.stringify(rows);
      // res.send(rows); 
      res.sendFile(path.join(__dirname,'public',"ChatAppLogin.html"))       
         
    }
    else{
    rows=JSON.stringify(rows);
    res.sendFile(path.join(__dirname,'public',"ChatSuccessDashboard.html"))
    

    connection.end()
      }
    })
 }
)


//Route to Open Group Chat Page

app.get ('/grouplogin',(req,res)=> {
  var connection = getConn();


  var x=req.query.chatgroup;
  connection.connect()

  var checkgroup =("select id from chatgroup where id="+x);

  connection.query( checkgroup, function (err, rows, fields) {
    //if (err) throw err; 
    console.log(rows);

    if (rows.length==0 || err) {
      // rows={id:"ERROR"};
      // rows=JSON.stringify(rows);
      res.send(rows);        
      res.sendFile(path.join(__dirname,'public',"ChatSuccessDashboard.html"));   
    }
    else{
    rows=JSON.stringify(rows);
    res.sendFile(path.join(__dirname,'public',"ChatBox.html"))
    

    connection.end()
      }
    })
 }
)




//Now to create an insert request to enter a new message
app.get ('/newmsg',(req,res)=> {
  var connection = getConn();


  var x=req.query.message;
  x="'"+x+"'";
  var y= req.query.userid;
  var z= req.query.groupid;
  connection.connect()

  
  
    
  var abc="INSERT INTO msglog (message,time,userid,group_id) VALUES ("+x+",localtimestamp(),"+y+","+z+")";
  console.log(abc);
    
    connection.query( abc, function (err, rows, fields) {
      if (err) throw err; 
      console.log ("select users.username as username ,msglog.message,msglog.id as msgid as message from msglog inner join users on msglog.userid=users.id where group_id="+z+" order by msglog.id ASC")
      //console.log ("INSERT INTO msglog (message,time) VALUES ('"+x+"',localtimestamp())");
      connection.query( "select users.username as username ,msglog.message as message,msglog.id as msgid from msglog inner join users on msglog.userid=users.id where group_id="+z+" order by msglog.id ASC", function (err, rows, fields) {
        if (err) throw err; 
        else          
        res.send(rows);
        connection.end()
      })
    })
  })

  //Route to refresh the messaging app
  app.get ('/refresh',(req,res)=> {
    var connection = getConn();

  
    var y= req.query.userid;
    var z= req.query.groupid;
    connection.connect()
      
    var checkLastMessage="SELECT MAX(msglog.id) from msglog inner join chatgroup on msglog.group_id=chatgroup.id where group_id="+z;
    console.log(checkLastMessage);
      
      connection.query( checkLastMessage, function (err, rows, fields) {

        if (err) throw err; 

        connection.query( "select users.username as username ,msglog.message as message,msglog.id as msgid from msglog inner join users on msglog.userid=users.id where group_id="+z+" order by msglog.id ASC", function (err, rows, fields) {
          if (err) throw err; 
          else          
          res.send(rows);
          connection.end()
        })
      })
    })
  

  
//Login app
//Route to make the login page for chatting app and request run on the same server localhost
app.get ('/login',(req,res)=> {
  res.sendFile(path.join(__dirname,'public',"ChatAppLogin.html"))
})

//Route to Authenticate username and password.If matches: return user id and redirect to chatting page:
app.get ('/auth',(req,res)=> {
  var connection = getConn();


  var x=req.query.username;
  var y= req.query.password;
  connection.connect()
  
    console.log("SELECT id from users where username='"+x+"'AND password='"+y+"'");
    connection.query( "SELECT id from users where username='"+x+"'AND password='"+y+"'", function (err, rows, fields) {
      if (err) {
        rows={id:"ERROR"};
        
      } 
    rows=JSON.stringify(rows);
    res.send(rows);
      connection.end()
      })
    }
)
    

app.listen(port, () => {
console.log(`Example app listening at http://localhost:${port}`)
})