Chat Application

Please find screenshots inside the screenshots folder

Tech Stack
Frontend : Vanilla JS, HTML, Css
Backend : Node.js, Express.js
Database : Mysql

Steps to install
1) Extract data from the zip file
2) Install Node
3) Install mysql
4) Install npm
5) Create a database "msgapp" with the following credentials
        host : localhost
        username : abhas
        password : Abcd@1234
        database : msgapp
6) Import the database file
    - Go to the root directory of the project
    - Run -> mysql -uabhas -pAbcd@1234 msgapp < dump.sql
    The database schema is imported
7) Go to the base directory of the project
8) Run npm install. This will install all the required dependencies
9) To run the application run -> node index.js
10) The app is now running on this URL http://localhost:3000/login